// This is an "add-on" of sorts that works ON TOP OF the arkenfox user.js file
// Use https://github.com/arkenfox/user.js/blob/master/user.js along with prefsCleaner & updater

user_pref("_user-overrides.js.parrot", "// Misc settings");
user_pref("browser.startup.page", 3);					// Resume previous session
user_pref("general.smoothScroll.msdPhysics.enabled", true);		// Better scrolling
user_pref("keyword.enabled", true);					// Use URL bar as search bar
user_pref("layout.frame_rate", 75);					// Target 75fps
user_pref("media.av1.enabled", false);					// Disable AV1 as we can't VAAPI it
user_pref("media.eme.enabled", true);					// Keep DRM enabled
user_pref("media.ffmpeg.vaapi.enabled", true);				// Enable VAAPI
user_pref("middlemouse.paste", false);					// Disable middle click to paste
user_pref("network.dns.disableIPv6", false);				// Re-enable IPv6 because it breaks localhost otherwise
user_pref("privacy.resistFingerprinting.letterboxing", false);		// Respect my window size
user_pref("toolkit.legacyUserProfileCustomizations.stylesheets", true);	// Enable userChrome.css
user_pref("webgl.disabled", false);					// Keep WebGL enabled
user_pref("xpinstall.signatures.required", false);			// Disable addon signaure requirements
user_pref("privacy.webrtc.legacyGlobalIndicator", false);		// Disable WebRTC window that doesn't work well with tiling

user_pref("_user-overrides.js.parrot", "// Stop the built-in password manager from nagging us");
user_pref("signon.rememberSignons", false);
user_pref("extensions.formautofill.addresses.enabled", false);
user_pref("extensions.formautofill.creditCards.enabled", false);
user_pref("extensions.formautofill.heuristics.enabled", false);

user_pref("_user-overrides.js.parrot", "// Keep cookies, history & localStorage");
user_pref("privacy.clearOnShutdown.cookies", false); 
user_pref("privacy.clearOnShutdown.history", false);
user_pref("privacy.clearOnShutdown.offlineApps", false);

user_pref("_user-overrides.js.parrot", "// github.com/yokoffing/betterfox --- Fastfox");
user_pref("browser.sessionstore.interval", 60000); // mainly to reduce SSD writes (fastfox default = 30000)
user_pref("content.notify.interval", 100000);
user_pref("gfx.webrender.precache-shaders", true);
user_pref("image.mem.decode_bytes_at_a_time", 262144); // alt=65536 (fastfox default = 131072)
user_pref("image.mem.shared.unmap.min_expiration_ms", 120000);
user_pref("network.buffer.cache.count", 240); // (fastfox default = 128)
user_pref("network.buffer.cache.size", 327680); // (fastfox default = 262144)
user_pref("network.http.max-connections", 1800);
user_pref("network.http.max-persistent-connections-per-server", 10);
user_pref("network.ssl_tokens_cache_capacity", 32768);
user_pref("nglayout.initialpaint.delay", 0);
user_pref("nglayout.initialpaint.delay_in_oopif", 0);
// user_pref("gfx.webrender.compositor", true); // FEATURE_FAILURE_DISABLE_RELEASE_OR_BETA: Cannot be enabled in release or beta
// user_pref("layers.gpu-process.enabled", true); // FEATURE_FAILURE_WAYLAND: Wayland does not work in the GPU process

// Don't REALLY want to mess around with caching unless it saves SSD writes
// or is really fast or something about ram use.
// TODO: test these out 
user_pref("browser.cache.memory.max_entry_size", 327680); // (fastfox default = 153600)
user_pref("gfx.canvas.accelerated.cache-items", 32768);
user_pref("gfx.canvas.accelerated.cache-size", 4096);
user_pref("gfx.content.skia-font-cache-size", 80);
user_pref("image.cache.size", 10485760);
user_pref("media.cache_readahead_limit", 9000);
user_pref("media.cache_resume_threshold", 6000);
user_pref("media.memory_cache_max_size", 1048576); // arkenfox lowers this
user_pref("media.memory_caches_combined_limit_kb", 3145728); // (fastfox default = 2560000)

user_pref("_user-overrides.js.parrot", "// github.com/yokoffing/betterfox --- Peskyfox");
user_pref("full-screen-api.transition-duration.enter", "0 0");
user_pref("full-screen-api.transition-duration.leave", "0 0");
user_pref("full-screen-api.warning.delay", -1);
user_pref("full-screen-api.warning.timeout", 0);
user_pref("gfx.webrender.quality.force-subpixel-aa-where-possible", true); // possible performance impact
user_pref("ui.SpellCheckerUnderlineStyle", 1); // 1 = dotted, https://kb.mozillazine.org/Ui.SpellCheckerUnderlineStyle#Possible_values_and_their_effects

user_pref("_user-overrides.js.parrot", "// github.com/yokoffing/betterfox --- Smoothfox");
user_pref("mousewheel.default.delta_multiplier_y", 300);

user_pref("_user-overrides.js.parrot", "// Uncomment in case of breakage.");
// Start from the top and work your way down until your breakage is fixed.

//user_pref("javascript.use_us_english_locale", true);	// IME support
//user_pref("network.http.referer.XOriginPolicy", 0);	// Relaxed "Referer" rules
//user_pref("privacy.resistFingerprinting", false);	// Stop resisting fingerprinting
	// "use CanvasBlocker with canvas and audio (the rest is not needed)"

user_pref("_user-overrides.js.parrot", "// ALL FINE");
